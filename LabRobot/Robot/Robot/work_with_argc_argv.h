#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>

void print_help();
void work_with_argc_argv(int argc, char* argv[], std::ifstream& fin, std::ofstream& fout, std::string& topology, unsigned int& limit);
