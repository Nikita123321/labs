#pragma once
#include "Field.h"
#include "Surface.h"
#include <vector>

class Tor : public Field
{
public:
	unsigned int calc_lenght_for_tor(Point point);
	unsigned int move(Point point);
	std::vector<std::tuple<Point, unsigned int>> lookup();
};