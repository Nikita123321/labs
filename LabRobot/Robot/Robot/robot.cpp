#include "robot.h"
#include "planar.h"
#include "cylinder.h"
#include "tor.h"
#include <cmath>
#include <climits>

std::vector<Point> Robot::get_path(Field& current_surface, unsigned int limit)
{
	Point current = current_surface.get_start();
	unsigned int number_of_point = 0;
	std::vector<Point> path;
	path.resize(number_of_point + 1);
	path[number_of_point] = current;
	unsigned int current_lenght = 0;
	unsigned int min;
	lenght = current_surface.get_length();
	std::vector<std::tuple<Point, unsigned int>> neighbors;
	while (lenght != 0)
	{
		min = UINT_MAX;
		neighbors = current_surface.lookup();
		if (neighbors.size() == 0)
		{
			throw NoWay("No way");
		}
		for (unsigned int i = 0; i < neighbors.size(); i++)
		{
			if (std::get<1>(neighbors[i]) < min)
			{
				min = std::get<1>(neighbors[i]);
				current = std::get<0>(neighbors[i]);
			}
		}
		try
		{
			lenght = current_surface.move(current);
			path.resize(++number_of_point + 1);
			path[number_of_point] = current;
		}
		catch (BadMove e)
		{
			std::cerr << "Bad move from point(" << e.badmove[0] << "," << e.badmove[1] << ") to point(" << e.badmove[2] << "," << e.badmove[3] << ")" << std::endl;
			std::cerr << "You go out of range";
			return path;
		}
		current_lenght++;
		if (current_lenght == limit)
		{
			throw NoWay("Your path is longer than the limit");
		}

		return path;
	}
}



