#include "Field.h"

void Field::install_field(std::istream& in)
{
	int i = 0;
	std::string s;
	while (std::getline(in, s))
	{
		area.resize(i + 1);
		for (unsigned int j = 0; j < s.length(); j++)
		{
			area[i].push_back(s[j]);
			if (s[j] == 'F')
			{
				finish.x = i;
				finish.y = j;
			}
			if (s[j] == 'S')
			{
				start.x = i;
				start.y = j;
				area[start.x][start.y] = '*';
			}
		}
		i++;
	}
	cols = s.length();
	rows = i;
	current_point = start;
	lenght = abs_for_unsigned_int(finish.x, start.x) + abs_for_unsigned_int(finish.y, start.y);
}


Point Field::get_start()
{
	return start;
}

unsigned int Field::get_length()
{
	return lenght;
}

void Field::change_length(unsigned int new_lenght)
{
	lenght = new_lenght;
}

std::vector <std::vector<char>> Field::get_area()
{
	return area;
}

void Field::print_field(std::ostream& out) const
{
	for (unsigned int i = 0; i < area.size(); i++)
	{
		for (unsigned int j = 0; j < area[i].size(); j++)
		{
			out << area[i][j];
		}
		out << "\n";
	}
}

Field* Field::get_class(std::string name_of_class)
{
	if (name_of_class == "Tor")
	{
		return new Tor();
	}
	if (name_of_class == "Cylinder")
	{
		return new Cylinder();
	}
	return new Planar();

}

std::istream& operator>> (std::istream& in, Field& current_field)
{
	current_field.install_field(in);
	return in;
}


std::ostream& operator<< (std::ostream& out, const Field& current_field)
{
	current_field.print_field(out);
	return out;
}

unsigned int abs_for_unsigned_int(unsigned int a, unsigned int b)
{
	if (a >= b){
		return a - b;
	}
	else{
		return b - a;
	}

}