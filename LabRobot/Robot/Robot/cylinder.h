#pragma once
#include "Field.h"
#include "Surface.h"



class Cylinder : public Field {
public:
	unsigned int calc_lenght_for_cylinder(Point point);
	unsigned int move(Point point);
	std::vector<std::tuple<Point, unsigned int>> lookup();
};
