#include "robot.h"
#include "work_with_argc_argv.h"


int main(int argc, char* argv[])
{
	unsigned int limit = 1000;
	std::ifstream fin("space.txt");
	if(!fin.is_open())
		std::cout << "dsg";
	std::ofstream fout("route.txt");
	std::string topology = "Planar";

	work_with_argc_argv(argc, argv,fin,fout,topology,limit);

	Field* current_surface = 0;
	current_surface = Field::get_class(topology);
	fin >> *(current_surface);

	Robot r;
	try
	{
		std::vector<Point> path;
		path = r.get_path(*current_surface, limit);
		fout << *(current_surface);
	}
	catch (NoWay e)
	{
		std::cerr << e.error << std:: endl;
	}

	delete current_surface;
	fin.close();
	fout.close();
	return 0;
}