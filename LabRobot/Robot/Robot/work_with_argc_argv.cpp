#include "work_with_argc_argv.h"

void print_help()
{
	std::cout << "You can use the following parameters " << std::endl;
	std::cout << "* use -h or --help for help " << std::endl;
	std::cout << "* use -l or --limit, if you want changelimit (default limit=1000)" << std::endl;
	std::cout << "* use -s or --space, if you want change input file (default space.txt) " << std::endl;
	std::cout << "* use -o or --out, if you want change output file (default route.txt) " << std::endl;
	std::cout << "* use -t or --topology, if you want change topology (default Planar) " << std::endl;
	exit(0);
}

void work_with_argc_argv(int argc, char* argv[], std::ifstream& fin, std::ofstream& fout, std::string& topology, unsigned int& limit)
{
	for (int i = 0; i < argc; i++)
	{
		if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
		{
			print_help();
		}
		if (!strcmp(argv[i], "-l") || !strcmp(argv[i], "--limit"))
		{
			limit = atoi(argv[++i]);
		}
		if (!strcmp(argv[i], "-s") || !strcmp(argv[i], "--space"))
		{
			fin.close();
			fin.open(argv[++i]);
			if (!fin.is_open())
				std::cout << "dsg";
		}
		if (!strcmp(argv[i], "-o") || !strcmp(argv[i], "--out"))
		{
			fout.close();
			fout.open(argv[++i]);
		}
		if (!strcmp(argv[i], "-t") || !strcmp(argv[i], "--topology"))
		{
			topology = argv[++i];
		}
	}
}