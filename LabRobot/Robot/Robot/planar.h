#pragma once
#include "Field.h"
#include "Surface.h"
#include <vector>

class Planar : public Field
{
public:
	unsigned int calc_lenght(Point point);
	unsigned int move(Point point);
	std::vector<std::tuple<Point, unsigned int> > lookup();
};


