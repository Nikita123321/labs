#include "planar.h"

unsigned int Planar::move(Point point)
{
	if ((point.x >= rows) || (point.y >= cols))
	{
		throw BadMove(current_point, point);
	}
	current_point = point;
	area[point.x][point.y] = '*';
	return calc_lenght(point);
}

unsigned int Planar::calc_lenght(Point point)
{
	return abs_for_unsigned_int(finish.x, point.x) + abs_for_unsigned_int(finish.y, point.y);
}

std::vector<std::tuple<Point, unsigned int>> Planar::lookup()
{
	std::vector<std::tuple<Point, unsigned int>> variant;
	int i = 0;
	if (((current_point.x + 1) < rows) && (area[current_point.x + 1][current_point.y] != '#') && (area[current_point.x + 1][current_point.y] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x + 1, current_point.y);
		variant[i-1] = std::make_tuple(new_point, calc_lenght(new_point));
	}
	if ((static_cast<int>(current_point.x - 1) >= 0) && (area[current_point.x - 1][current_point.y] != '#') && (area[current_point.x - 1][current_point.y] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x - 1, current_point.y);
		variant[i-1] = std::make_tuple(new_point, calc_lenght(new_point));
	}
	if (((current_point.y + 1) < cols) && (area[current_point.x][current_point.y + 1] != '#') && (area[current_point.x][current_point.y + 1] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x, current_point.y + 1);
		variant[i-1] = std::make_tuple(new_point, calc_lenght(new_point));
	}
	if ((static_cast<int>(current_point.y - 1) >= 0) && (area[current_point.x][current_point.y - 1] != '#') && (area[current_point.x][current_point.y - 1] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x, current_point.y - 1);
		variant[i-1] = std::make_tuple(new_point, calc_lenght(new_point));
	}
	return variant;
}