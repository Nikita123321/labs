#pragma once
#include "Surface.h"
#include "planar.h"
#include "cylinder.h"
#include "tor.h"

class Field : public Surface
{
private:
	void install_field(std::istream& in);
	void print_field(std::ostream& out) const;
	void change_length(unsigned int new_lenght);
public:
	Field() {};
	friend std::istream& operator>> (std::istream&, Field&);
	friend std::ostream& operator<< (std::ostream&, const Field&);
	Point get_start();
	unsigned int get_length();
	std::vector <std::vector<char>> get_area();
	static Field* get_class(std::string);
protected:
	Point start;
	Point finish;
	Point current_point;
	unsigned int lenght;
	unsigned int rows;
	unsigned int cols;
	std::vector < std::vector<char> > area;
};

unsigned int abs_for_unsigned_int(unsigned int a, unsigned int b);