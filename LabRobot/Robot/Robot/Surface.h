#pragma once
#include "Field.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <tuple>
#include <string>


class Point
{
public:
	Point() {};
	Point(unsigned int a, unsigned int b);
	unsigned int x;
	unsigned int y;
};

class NoWay : public std::exception
{
public:
	NoWay(std::string message);
	std::string error;
};

class BadMove : public std::exception
{
public:
	BadMove(Point from, Point to);
	unsigned int badmove[4];
};

class Surface
{
public:
	virtual unsigned int move(Point point) = 0;
	virtual std::vector<std::tuple<Point, unsigned int>> lookup() = 0;
};
