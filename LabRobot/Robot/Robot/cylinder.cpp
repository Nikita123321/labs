#include "cylinder.h"

unsigned int Cylinder::move(Point point)
{
	if ((point.x >= rows) || (point.y >= cols))
	{
		throw BadMove(current_point, point);
	}
	current_point = point;
	area[point.x][point.y] = '*';
	return calc_lenght_for_cylinder(point);

}

unsigned int Cylinder::calc_lenght_for_cylinder(Point point)
{
	if (abs_for_unsigned_int(finish.y, point.y) < (cols - abs_for_unsigned_int(finish.y, point.y)))
	{
		return abs_for_unsigned_int(finish.x, point.x) + abs_for_unsigned_int(finish.y, point.y);
	}
	else
	{
		return abs_for_unsigned_int(finish.x, point.x) + cols - abs_for_unsigned_int(finish.y, point.y);
	}
}

std::vector<std::tuple<Point, unsigned int>> Cylinder::lookup()
{
	std::vector<std::tuple<Point, unsigned int>> variant;
	int i = 0;
	if (((current_point.x + 1) < rows) && (area[current_point.x + 1][current_point.y] != '#') && (area[current_point.x + 1][current_point.y] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x + 1, current_point.y);
		variant[i - 1] = std::make_tuple(new_point, calc_lenght_for_cylinder(new_point));
	}
	if ((static_cast<int>(current_point.x - 1) >= 0) && (area[current_point.x - 1][current_point.y] != '#') && (area[current_point.x - 1][current_point.y] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x - 1, current_point.y);
		variant[i - 1] = std::make_tuple(new_point, calc_lenght_for_cylinder(new_point));
	}
	if (((current_point.y + 1) < cols) && (area[current_point.x][current_point.y + 1] != '#') && (area[current_point.x][current_point.y + 1] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x, current_point.y + 1);
		variant[i - 1] = std::make_tuple(new_point, calc_lenght_for_cylinder(new_point));
	}
	if ((static_cast<int>(current_point.y - 1) >= 0) && (area[current_point.x][current_point.y - 1] != '#') && (area[current_point.x][current_point.y - 1] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x, current_point.y - 1);
		variant[i - 1] = std::make_tuple(new_point, calc_lenght_for_cylinder(new_point));
	}
	if (((current_point.y + 1) == cols) && (area[current_point.x][0] != '#') && (area[current_point.x][0] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x, 0);
		variant[i - 1] = std::make_tuple(new_point, calc_lenght_for_cylinder(new_point));
	}
	if ((static_cast<int>(current_point.y - 1) == -1) && (area[current_point.x][cols - 1] != '#') && (area[current_point.x][cols - 1] != '*'))
	{
		variant.resize(++i);
		Point new_point(current_point.x, cols - 1);
		variant[i - 1] = std::make_tuple(new_point, calc_lenght_for_cylinder(new_point));
	}

	return variant;
}