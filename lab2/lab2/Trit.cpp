#include "Trits.h"

using namespace Trits;

// ������ ������ Trit;

Tritset :: Trit :: Trit (size_t pos, Tritset &set) {
	this->pos = pos;
	this->orig = &set;
}

Tritset::Trit :: ~Trit() {
	this->pos = 0;
	this->orig = NULL;
}

size_t Tritset::Trit::getPos(size_t pos) {
	size_t mem = (size_t)((double)pos / 4 + 0.75);
	return mem;
}

Trits::Tritset::Trit& Trits::Tritset::Trit::operator= (trits value) {
	if (this->orig->tritQuant < this->pos) 
		if (value)
			this->orig->resize(pos);
		else
			return *this;

	char shift = (3 - pos % 4) * 2;
	unsigned int byte = (unsigned int)((double)(((double)pos - 3) / 4 + 0.75));
	char buf = 3;
	buf = buf << shift;
	buf = ~buf;
	this->orig->cont[byte] = this->orig->cont[byte]  & buf;
	buf = 0;
	buf |= (char)value;
	buf = buf << shift;
	this->orig->cont[byte] = this->orig->cont[byte] | buf;
	return *this;
}

Trits::Tritset::Trit& Trits::Tritset::Trit::operator= (const Trit& newTrit) {
	if (True == newTrit)
		*this = True;
	else
		if (newTrit == False)
			*this = False;
		else
			*this = Unknown;
	return *this;
}


// ������ ������ Tritset;

Trits::Tritset::Tritset() {
	this->cont = NULL;
	this->tritQuant = 0;
	this->arrSize = 0;
}

Trits::Tritset::Tritset(size_t size) {
	size_t mem = (size_t)((double)size / 4 + 0.75);
	this->cont = (char *)calloc(mem, sizeof(char));
	this->tritQuant = size;
	this->arrSize = mem;
}

Trits::Tritset::Tritset (const Tritset& newTritset) {
	this->arrSize = newTritset.arrSize;
	this->tritQuant = newTritset.tritQuant;
	size_t mem = (size_t)((double)this->tritQuant / 4 + 0.75);
	this->cont = (char*)calloc(mem, sizeof(char));
	for (size_t i = 0; i < mem; i++)
		this->cont[i] = newTritset.cont[i];
}

Trits::Tritset::Tritset(Tritset &&newTritset)
	:cont(NULL),
	tritQuant(0),
	arrSize(0)
{
	this->tritQuant = newTritset.tritQuant;
	this->arrSize = newTritset.arrSize;
	this->cont = newTritset.cont;
	newTritset.arrSize = 0;
	newTritset.tritQuant = 0;
	newTritset.cont = NULL;
}

Trits::trits Tritset::getVal(size_t index) {
	trits v = Unknown;
	char buf = 3;
	if (index >= this->tritQuant)
		return v;
	char shift = (3 - index % 4) * 2;
	unsigned int byte = (unsigned int)(double)((index) / 4 + 0.75);
	buf = buf << shift;
	buf = buf & this->cont[byte];
	buf = buf >> shift;
	if (buf)
		if (buf == 1)
			v = False;
		else
			v = True;

	return v;
}

void Tritset::resize(size_t new_size) {
	/*if ((*this).length() == 0) {
		free(this->cont);
		this->cont = NULL;
		this->arrSize = 0;
		this->tritQuant = 0;
		return;
	}*/
	size_t mem = (size_t)((double)(new_size + 1) / 4 + 0.75);
	size_t old_mem = (*this).capacity();
	this->cont = (char *)realloc(this->cont, sizeof(char) * mem);
	if (mem > old_mem)
		memset(this->cont + old_mem, 0, mem - old_mem);
	(*this).tritQuant = new_size + 1;
	this->arrSize = mem;
	return;
}

Trits::Tritset::~Tritset() {
	free(this->cont);
	this->cont = NULL;
	this->arrSize = 0;
	this->tritQuant = 0;
}

Trits::Tritset::Trit Trits::Tritset::operator[](size_t pos) {
	Trit newTrit(pos, *this);
	return newTrit;
}

size_t Trits::Tritset::�ardinality(trits value) {
	int counter = 0;
	int i = this->tritQuant - 1;
	if (value == Unknown)
		while ((i >= 0) && !((*this)[i] == True) && !((*this)[i] == False))
			i--;
	if (i == 0) return 0;
	for (int j = 0; j <= i; j++)
		if (this->operator[](j) == value) counter++;

	return counter;
}

size_t Trits::Tritset::capacity() {
	return (size_t)((double)this->tritQuant / 4 + 0.75);
}

size_t Trits::Tritset::length() {
	/*size_t p = ((*this).�ardinality(True) > (*this).�ardinality(False) ?
		(*this).�ardinality(True) : (*this).�ardinality(False));
	return p;*/
	size_t counter = 0;
	size_t i = 0;
	for (i = this->tritQuant - 1; i >= 0; i--)
		if (!(this->operator[](i) == Unknown))
			break;
	return i;
}

void Trits::Tritset::trim(size_t lastIndex) {
	for (int i = lastIndex; i < (int)this->tritQuant; i++)
		(*this)[i] = Unknown;
}

void Trits::Tritset::shrink() {
	(*this).resize((*this).length());
	return;
}

Trits::Tritset Trits::Tritset::operator&(Trits::Tritset &newTritset) {
	int nsize = (this->tritQuant > newTritset.tritQuant) ? this->tritQuant : newTritset.tritQuant;
	Tritset k(nsize);
	for (int i = 0; i < nsize; i++) {
		if (((*this)[i] == False) || (newTritset[i] == False))
			k[i] = False;
		else
			if (((*this)[i] == True) && (newTritset[i] == True))
				k[i] = True;
			else
				k[i] = Unknown;
	}
	return k;
}

Trits::Tritset& Trits::Tritset::operator=(const Trits::Tritset &newTritset) {
	Tritset tmp(newTritset.tritQuant);
	size_t mem = (*this).capacity();
	for (int i = 0; i < (int)mem; i++) {
		tmp.cont[i] = newTritset.cont[i];
	}
	std::swap(tmp.cont, this->cont);
	std::swap(tmp.tritQuant, this->tritQuant);

	return *this;
}

Trits::Tritset Trits::Tritset::operator|(Tritset &newTritset) {
	int nsize = (this->tritQuant > newTritset.tritQuant) ? this->tritQuant : newTritset.tritQuant;
	Tritset set(nsize);
	for (int i = 0; i < nsize; i++) {
		if (((*this)[i] == True) || (newTritset[i] == True))
			set[i] = True;
		else
			if (((*this)[i] == False) && (newTritset[i] == False))
				set[i] = False;
			else
				set[i] = Unknown;
	}
	return set;
}

Trits::Tritset Trits::Tritset::operator~() {
	Tritset k(this->tritQuant);
	for (int i = 0; i < (int)this->tritQuant; i++) {
		if ((*this)[i] == False)
			k[i] = True;
		else
			if ((*this)[i] == True)
				k[i] = False;
			else
				k[i] = Unknown;
	}
	return k;
}

bool Trits::operator==(Tritset &fTritset, Tritset &sTritset) {
	if (fTritset.tritQuant != sTritset.tritQuant) return false;
	for (int i = 0; i < (int)fTritset.tritQuant; i++) {
		if (fTritset[i] != sTritset[i]) return false;
	}
	return true;
}

Trits::Tritset::Trit::operator trits() const
{	
	trits value;
	value = this->orig->getVal(this->pos);
	return value;
}

trits Trits::operator& (const trits fVal, const trits sVal) {
	if ((fVal == False) || (sVal == False))
		return False;
	else
		if ((fVal == True) && (sVal == True))
			return True;
		else
			return Unknown;
}

trits Trits::operator| (const trits fVal, const trits sVal) {
	if ((fVal == True) || (sVal == True))
		return True;
	else
		if ((fVal == False) && (sVal == False))
			return False;
		else
			return Unknown;
}

trits Trits::operator~(const trits val) {
	if (val == True)
		return False;
	else
		if (val == False)
			return True;
		else
			return Unknown;
}

bool Trits::operator==(const trits fVal, const trits sVal) {
	if (((fVal == True) && (sVal == True)) || ((fVal == False) && (sVal == False)) || ((fVal == Unknown) && (sVal == Unknown)))
		return true;
	else
		return false;
}

bool Trits::operator!=(const trits fVal, const trits sVal) {
	if (fVal == sVal)
		return false;
	else
		return true;
}
