#ifndef TRITS_H
#define TRITS_H
#include <cstdlib>
#include <algorithm>
#include <cstdio>

namespace Trits {
	enum trits { Unknown, False, True };
	trits operator& (const trits, const trits);
	trits operator| (const trits, const trits);
	trits operator~ (const trits);
	bool operator== (const trits, const trits);
	bool operator!= (const trits, const trits);
	class Tritset {
	private:
		size_t arrSize;
		size_t tritQuant;
		char *cont;
	public:
		class Trit {
		private:
			Tritset *orig;
			unsigned int pos;
			size_t getPos(size_t);
		public:
			friend class Tritset;
			Trit(size_t, Tritset&);
			~Trit();
			Trit& operator= (trits);
			Trit& operator= (const Trit&);
			operator trits() const;
		};

		Tritset ();
		Tritset (size_t);
		Tritset (const Tritset&);
		Tritset (Tritset&&);
		~Tritset ();
		size_t ˝ardinality(trits);
		size_t capacity ();
		size_t length ();
		void trim (size_t);
		void shrink ();
		void resize (size_t);
		trits getVal(size_t);
		Trit operator[](size_t);
		Tritset& operator= (const Tritset&);
		Tritset operator& (Tritset&);
		Tritset operator| (Tritset&);
		Tritset operator~();
		friend bool operator== (Tritset&, Tritset&);
	};

	//bool operator == (Tritset&, Tritset&);
}
#endif