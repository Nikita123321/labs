#include "gtest/gtest.h"
#include "Trits.h"

using namespace Trits;

class tritsettest : public ::testing::Test {};

TEST_F(tritsettest, testMemoryOperations) {
	Tritset set(1000);
	// length of internal array
	size_t allocLength = set.capacity();
	assert(allocLength >= 1000 * 2 / 8 / sizeof(char));
	// 1000*2 - min bits count
	// 1000*2 / 8 - min bytes count
	// 1000*2 / 8 / sizeof(uint) - min uint[] size


	//�� �������� ������� ������
	//set[1000000000] = Unknown;
	set[1000] = Unknown;
	assert(allocLength == set.capacity());

	// false, but no exception or memory allocation

	if ((trits)(set[2000000]) == True) {}
	assert(allocLength == set.capacity());

	//��������� ������
	set[1000] = False;
	set[2000] = True;
	assert(allocLength < set.capacity());


	//no memory operations
	allocLength = set.capacity();
	set[1000000000] = Unknown;
	set[100] = False;
	assert(allocLength == set.capacity());


	//������������ ������ �� ���������� �������� ��� 
	//�� �������� ������������ ��� �������� ���������� �������������� �����
	//� ������ ������ ��� ����� 1000�000
	set.shrink();
	assert(allocLength >= set.capacity());
}

TEST_F(tritsettest, testLogicOperations)
{
	Tritset setA(10);
	Tritset setB(20);
	//trits tt = setA[5];
	Tritset setC = setA & setB;
	assert(setC.capacity() == setB.capacity());

	for (int i = 0; i < 9; i++) {
		setA[i] = (trits)(i / 3);
		setB[i] = (trits)(i % 3);
	}

	for (int i = 0; i < 3; i++) {
		setC[i] = setA[i] & setB[i];
	}

}

int main(int argc, char *argv[]) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}