#ifndef TRIT_H
#define TRIT_H

#include <cstdlib>
#include <algorithm>
#include <cstdio>

namespace TritNamespace {
	enum trites { Unknown, False, True };
	class Trit {
	private:
		char *trits;
		size_t size;
		size_t tritQuant;
	public:
		class SingleTrit {
		private:
			char *value;
			unsigned int pos;
			size_t *setSize;
			char *setData;
		public:
			SingleTrit(size_t, Trit&);
			~SingleTrit();
			friend class Trit;
			SingleTrit &operator=(trites);
			SingleTrit &operator=(const SingleTrit&);
			trites operator&(const SingleTrit&);
			trites operator|(const SingleTrit&);
			trites operator~();
			friend bool operator==(const SingleTrit&, const SingleTrit&);
			friend bool operator==(const SingleTrit&, const trites);
			friend bool operator!=(const SingleTrit&, const SingleTrit&);
			
		};

		Trit(size_t);
		~Trit();
		friend class SingleTrit;
		SingleTrit& operator[] (size_t);
		Trit& operator= (const Trit&);
		Trit& operator& (Trit& const);
		Trit& operator| (Trit& const);
		Trit& operator~ ();	
		friend bool operator== (Trit& const, Trit& const);
		
		int tritQuantity() {
			return this->tritQuant;
		}
		size_t cardinality(enum trites);
		size_t length();
		void shrink();
		void trim(size_t lastIndex);
		size_t capacity();
	};

	bool operator==(const Trit :: SingleTrit&, const Trit :: SingleTrit&);
	bool operator==(const Trit :: SingleTrit&, const trites);
	bool operator!=(const Trit :: SingleTrit&, const Trit :: SingleTrit&);
}

size_t getPos(int);
size_t maxSize(int, int);

#endif