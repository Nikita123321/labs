#include <string>
#include <fstream>
#include <cstdio>
#include <list>

using namespace std;

int main(int argc, char* argv[]) {
	string buf;
	
	std::ifstream fin;
	fin.open(argv[1]);

	std::list<string> storage;
	

	while (!fin.eof()) {
		getline(fin, buf);
		storage.push_back(buf);
	}

 	fin.close();

	storage.sort();
	ofstream fout("output.txt");

	std::list<string> ::iterator it = storage.begin();
	
	for (; it != storage.end(); it++)
		fout << *it << endl;

	fout.close();

	return 0;
	
}